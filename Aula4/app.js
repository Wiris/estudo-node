const express = require("express");
const app = express();

app.get("/", function (req, res) {
    res.send("<h1>Bem Vindo!</h1>");
});

// Query params: ?nome=valor
app.get("/canal", function (req, res) {
    let canal = req.query["canal"];
    if (canal) {
        res.send("<h1>Bem Vindo ao canal " + canal + " !</h1>");
    } else {
        res.send("<h1>Bem Vindo ao canal!</h1>");
    }
});

// parâmetros opcionais 
app.get("/blog/:artigo?", function (req, res) {
    let artigo = req.params.artigo;
    if (artigo) {
        res.send("<h1>Bem Vindo ao Meu Blog!<br>Artigo: " + artigo + "</h1>");
    } else {
        res.send("<h1>Bem Vindo ao Meu Blog!</h1>");
    }
});

// parâmetros fixos 
app.get("/ola/:nome/:empresa", function (req, res) {
    res.send("<h1>Oi " + req.params.nome + " da " + req.params.empresa + " !</h1>");
});

app.listen(5000, function (erro) {
    if (erro) {
        console.log("Ocorreu um erro");
    } else {
        console.log("Servidor iniciado com sucesso");
    }
})