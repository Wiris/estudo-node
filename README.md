# Estudos com Node.js

Instalação do Express

`npm install express --save`

Instalação do Nodemon

`npm install nodemon -g`

Instalação do EJS 

`npm install ejs --save`

Instalação do  body-parser

`npm install body-parser --save`

Instação do Sequelize

`npm install --save sequelize`

Dependencia do Sequelize para o Mysql

`npm install --save mysql2` 

Instalação do Dotenv

`npm install dotenv --save`

